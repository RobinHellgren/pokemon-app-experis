import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CataloguePage } from './pages/CataloguePage/catalogue-page.component';
import { LandingPage } from './pages/LandingPage/landing-page.component';
import { TrainerPage } from './pages/TrainerPage/trainer-page.component';
import { PokemonService } from './services/PokemonService';



@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    TrainerPage,
    CataloguePage,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [PokemonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
