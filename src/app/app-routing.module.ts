import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingPage } from "./pages/LandingPage/landing-page.component";
import { TrainerPage } from "./pages/TrainerPage/trainer-page.component";
import { CataloguePage } from "./pages/CataloguePage/catalogue-page.component";
import { AuthGuardLandingPageService } from "./services/AuthGuardLandingPageService";
import { AuthGuardLoggedInPagesService } from "./services/AuthGuardLoggedInPagesService";


const routes: Routes = [   
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'landing'
    },    
    {
        path: 'landing',
        component: LandingPage,
        canActivate: [AuthGuardLandingPageService]    
    },
    {
        path: 'trainer',
        component: TrainerPage,
        canActivate: [AuthGuardLoggedInPagesService]    
    },
    {
        path: 'catalogue',
        component: CataloguePage,
        canActivate: [AuthGuardLoggedInPagesService]
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuardLandingPageService, AuthGuardLoggedInPagesService]
})

export class AppRoutingModule {

}