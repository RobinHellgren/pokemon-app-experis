import { Component, inject } from "@angular/core";
import { INamedApiResource, ITypePokemon } from "pokeapi-typescript";
import { Pokemon, PokemonService } from "src/app/services/PokemonService";
import {Router} from '@angular/router';


@Component({
    selector: 'app-catalogue-page',
    templateUrl: './catalogue-page.component.html',
    styleUrls: ['./catalogue-page.component.css']
})



export class CataloguePage {
    pokemonCatalogueList: Pokemon[] = [];

    constructor(private router: Router, private pokeService: PokemonService) { }

    /** Reads all caught pokemon from local storage and then takes all pokemon from the API */
    ngOnInit() {
        let caughtPokemonString: string | null = localStorage.getItem('pokemon');
        let caughtPokemonArray: Array<string>;
        
        if (!caughtPokemonString) {
            caughtPokemonArray = [];
        } else {
            caughtPokemonArray = JSON.parse(caughtPokemonString);
        }
        this.pokemonCatalogueList = this.pokeService.getPokemon(caughtPokemonArray)
    }
    /** Adds clicked pokemon to localstorage (if it's not already caught). And updates the pokemon list */
    addPokemon(name: string) {

        let pokemon: string | null = localStorage.getItem('pokemon');
        let pokemonArray: Array<string>;

        if (!pokemon) {
            pokemonArray = [];
        } else {
            pokemonArray = JSON.parse(pokemon);
        }
        if (pokemonArray.includes(name)){
            alert("You've already caught that one!")
        }
        else {
        pokemonArray.push(name);
        let itemIndex = this.pokemonCatalogueList.findIndex(pokemon => pokemon.name === name)
        this.pokemonCatalogueList[itemIndex].caught = true 
        localStorage.setItem('pokemon', JSON.stringify(pokemonArray));
        }
    }
    /** Redirects the user to the trainer page */
    redirectToTrainer(): void {
        this.router.navigate(['/trainer']);
    }
}