import { Component, OnInit } from "@angular/core";
import {Router} from '@angular/router';

@Component({
    selector: 'app-trainer-page',
    templateUrl:  './trainer-page.component.html',
    styleUrls: ['./trainer-page.component.css']
})



export class TrainerPage implements OnInit{
    
    name;
    pokemon;

    constructor(private router: Router) {}
    /** Get the username and the pokemons from localstorage  */
    ngOnInit(): void {
        this.name = localStorage.getItem('userName');

        let temp = localStorage.getItem('pokemon');

        if(temp !== null)
        this.pokemon = JSON.parse(temp);
    }
    /**Redirects the user to the catalogue page */
    redirectToCatalogue(): void {
        this.router.navigate(['/catalogue']);
    }
    /**Clears local storage and redirects the user to the startpage*/
   logout(): void {
       localStorage.clear();
        this.router.navigate(['/landing']);
    }

}