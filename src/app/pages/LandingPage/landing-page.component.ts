import { Component } from "@angular/core";
import {Router} from '@angular/router';

@Component({
    selector: 'app-landing-page',
    templateUrl:  './landing-page.component.html',
    styleUrls: ['./landing-page.component.css']
})



export class LandingPage {

    constructor(private router: Router) {}

     value = '';
    /** Updates the value that is sent to localstorage for the login */
    onKey(event) {
        this.value = event.target.value;
    }
    /** Saves the username in localstorage and sends the user to the catalogue page */
    login(): void {
        localStorage.setItem('userName', this.value);
        this.router.navigate(['/catalogue']);
    }
  
}