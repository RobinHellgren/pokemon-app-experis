import { Injectable } from "@angular/core";
import PokeAPI from "pokeapi-typescript";

export interface Pokemon {
    name: string,
    imgUrl: string,
    detailsUrl: string
    caught: boolean
}


@Injectable({

    providedIn: 'root'

})


export class PokemonService {

    
    constructor(){
        
    }
    

    /** Gets the pokemon from the API and constructs the picture URL for the sprites. It also checks if the pokemon is caught */
    getPokemon(caughtPokemon: string[]) : Pokemon[]{
        let pokemon: Pokemon[] = [];
        PokeAPI.Pokemon.list(1000, 0).then(response => {
            response.results.forEach(row => {
                let urlAsChar = Array.from(row.url)
                let nrAsChars = urlAsChar.slice(34,urlAsChar.length-1).join("")
                let nr = parseInt(nrAsChars)
                let poke = {
                    name: row.name,
                    imgUrl: `/assets/${nr}.png`,
                    detailsUrl: row.url,
                    caught: false
                }
                if(caughtPokemon.includes(row.name)){
                    poke.caught = true
                }
                pokemon.push(poke);
            });
        })
        return pokemon;
    }

}