import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthGuardLandingPageService implements CanActivate {
    constructor(private router: Router) { }
    /** Checks if the user is logged in - if the user is logged in they are redirected to the catalogue*/
    canActivate() {
        if (!(localStorage.getItem('userName'))) {
            return true;
        }
        this.router.navigate(['/catalogue']);
        return false;
    }
}