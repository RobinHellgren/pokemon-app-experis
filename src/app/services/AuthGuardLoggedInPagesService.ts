import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthGuardLoggedInPagesService implements CanActivate {
    constructor(private router: Router) {}
    /** Checks if the user is logged in - if they're not - they are redirected to the startpage*/
    canActivate() {
        if (localStorage.getItem('userName')) {
            return true;
        }
        this.router.navigate(['/landing']);
        return false;
    }
}